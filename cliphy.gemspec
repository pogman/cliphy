# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'cliphy/version'

Gem::Specification.new do |spec|
  spec.name          = "cliphy"
  spec.version       = "0.0.1"
  spec.platform      = Gem::Platform::Ruby
  spec.licenses      = ['MIT']
  spec.authors       = ["Murilo Paixão"]
  spec.email         = ["murilo.paixao.2@gmail.com"]
  spec.homepage      = "https://github.com/pogist/cliphy"
  spec.summary       = "Cool ASCII stuff from giphy right on your terminal."
  spec.description   = "Cool ASCII stuff from giphy right on your terminal."

  spec.required_ruby_version = '>= 2.0.0'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
